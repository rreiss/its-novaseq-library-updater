# ITS NovaSeq Library Updater

this is a python script that will update plate of libraries with the Nova Seq runmode and give each library a new index.   It runs in conjunction of  the loop webdriver script and will read as input the output of the script (to get the container names of the plates that have moved to the pooling queue.
It also reads as input the new value of the indexes for each library.